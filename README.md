# Project Development

## Templating Engine
This project uses [Handlebars](https://handlebarsjs.com) templates to construct complex html.

See [Examples](https://github.com/pcardune/handlebars-loader/tree/main/examples) how to use it.

## Git
Use [Semantic Commit Messages](https://nitayneeman.com/posts/understanding-semantic-commit-messages-using-git-and-angular/)

## Run the app locally
In order to call backend from `localhost` in your chrome browser use [block-insecure-private-network-requests](https://stackoverflow.com/a/66555660) in `chrome://flags/` and enable `Allow invalid certificates for resources loaded from localhost`.

## Code Quality
## Testing
Put all your tests under `./test` folder.
