import Router from '../../src/page/router';
import expect from "expect";
describe('Router', () => {

    const mockedPushState = jest
        .spyOn(window.history, 'pushState')
        .mockImplementation(() => {});

    const mockedPopulateContainer = jest
        .spyOn(Router.prototype, '_populateContainer')
        .mockImplementation(() => {});

    it('should prepopulate DOM with Home Page', () => {
        //init
        const pageContainer = document.createElement('div');
        const routes = {
            '/': '<div>Home</div>',
            '/tournament': '<div>Tournament</div>'
        }

        //invoke
        new Router(pageContainer, routes);

        //check
        expect(mockedPopulateContainer).toHaveBeenCalledWith('<div>Home</div>');
    });

    it('should populate DOM with Tournament Page', () => {
        //init
        const pageContainer = document.createElement('div');
        const routes = {
            '/': '<div>Home</div>',
            '/tournament': '<div>Tournament</div>'
        }
        const router = new Router(pageContainer, routes);

        //invoke
        router.route('/tournament')

        //check
        expect(mockedPopulateContainer).toHaveBeenLastCalledWith('<div>Tournament</div>');
        expect(mockedPushState).toHaveBeenCalledWith({}, '/tournament', 'null/tournament')
    });
});