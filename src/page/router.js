
export default class Router {
    constructor(pageContainer, routes) {
        this.routes = routes;
        this.pageContainer = pageContainer;

        this._registerOnHistoryPop();
        this._populateContainer(this.routes['/'])
    }

    route(pathname) {
        window.history.pushState({}, pathname, window.location.origin + pathname);
        this._populateContainer(this.routes[pathname]);
    };

    _registerOnHistoryPop() {
        window.onpopstate = () => {
            this._populateContainer(this.routes[window.location.pathname])
        }
    }

    _populateContainer(html) {
        this.pageContainer.innerHTML = html;
    }
}