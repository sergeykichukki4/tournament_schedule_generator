import ServiceWorkerConfigService from './service/service-worker-config';
import Router from "./page/router";
import './page/main_page/main_page'

import mainPage from './page/main_page/main_page.hbs';
import tournamentPage from './page/tournament/tournament.hbs';

import './page/main_page/main_page.css'

export default class App {
    constructor() {
        const pageContainer = document.getElementById('page');
        const routes = {
            '/': mainPage(),
            '/tournament': tournamentPage()
        }
        this.router = new Router(pageContainer, routes);

        const mainPageLink = document.getElementById('main-page-link');
        mainPageLink.addEventListener('click', (e) =>{
            e.preventDefault();
            this.router.route('/');
        });

        const tournamentPageLink = document.getElementById('tournament-page-link');
        tournamentPageLink.addEventListener('click', (e) =>{
            e.preventDefault();
            this.router.route('/tournament');
        });


        if(process.env.WEBPACK_ENV === 'production') {
            this.serviceWorkerConfig = new ServiceWorkerConfigService();
        } else {
          //TODO
        }
    }
}
