const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const postCssNormalize = require('postcss-normalize');

const path = require('path');

module.exports = {
  entry: './src/main.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: Object.assign({}, undefined),
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  require('postcss-flexbugs-fixes'),
                  require('postcss-preset-env')({
                    autoprefixer: {
                      flexbox: 'no-2009',
                    },
                    stage: 3,
                  }),
                  postCssNormalize()
                ],
              }
            },
          },
        ],
      },      
      { 
        test: /\.js$/,
        exclude: /node_modules/
      },
      { 
        test: /\.(png|svg|jpg|jpeg|gif|map)$/i,
        type: 'asset/resource'
      },
      { test: /\.hbs$/, 
        loader: "handlebars-loader" 
      }  
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Schedule Generator',
      template: 'src/index.hbs'
    }),
    new CopyWebpackPlugin({
      patterns: [{from: 'src/icon', to: 'icon'}, {from: 'src/font', to: 'font'}]
    }),
    new MiniCssExtractPlugin()
  ]
};
